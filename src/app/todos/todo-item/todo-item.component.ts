import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Todo } from '../models/todo.model';
import { FormControl, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import * as actions from '../todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() todo: Todo;
  @ViewChild('inputRefered') txtInputRefered: ElementRef;

  chkCompleted: FormControl;
  txtInput: FormControl;

  editing: boolean = false;
  

  constructor( private store: Store<AppState> ) { }

  ngOnInit(): void {
    // this.todo.done = true;
    this.chkCompleted = new FormControl( this.todo.done );
    this.txtInput = new FormControl( this.todo.text, Validators.required );
    this.chkCompleted.valueChanges.subscribe( value => {
      // console.log(value);
      this.store.dispatch( actions.toggle({id: this.todo.id}) );
    } );
  }

  removeItem() {
    this.store.dispatch( actions.remove({ id: this.todo.id }) );
  }

  editItem() {
    this.editing = true;
    this.txtInput.setValue( this.todo.text );
    
    setTimeout( () => {
      this.txtInputRefered.nativeElement.select()
    }, 1);
  }

  finishEditing() {
    this.editing = false;

    if(this.txtInput.invalid || this.txtInput.value === this.todo.text) return;

    this.store.dispatch(
      actions.edit({
        id: this.todo.id,
        text: this.txtInput.value
      })
    );
  }

}
