import { createAction, props } from '@ngrx/store';

export const create = createAction(
  '[TODO] Create Todo',
  props<{text: string}>()
);

export const toggle = createAction(
  '[TODO] Toogle Todo',
  props<{id: number}>()
);

export const edit = createAction(
  '[TODO] Edit Todo',
  props<{id: number, text: string}>()
);
  
export const remove = createAction(
  '[TODO] Remove Todo',
  props<{id: number}>()
);

export const toogleAll = createAction(
  '[TODO] Toogle All Todo',
  props<{done: boolean}>()
)

export const clearCompleted = createAction('[TODO] Clear Completed Todo')