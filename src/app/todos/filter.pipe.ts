import { Pipe, PipeTransform } from '@angular/core';

import { validFilters } from '../filters/filter.actions';
import { Todo } from './models/todo.model';

@Pipe({
  name: 'todoFilter'
})
export class FilterPipe implements PipeTransform {

  transform(todos: Todo[], filter: validFilters): unknown {
    switch(filter) {
      case 'completed':
        return todos.filter( todo => todo.done );
      case 'pending':
        return todos.filter( todo => !todo.done );
      default:
        return todos;
    }
  }

}
