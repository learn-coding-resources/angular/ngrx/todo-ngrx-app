import { state } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as actions from '../../filters/filter.actions';
import { AppState } from '../../app.reducer';
import { clearCompleted } from '../todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css']
})
export class TodoFooterComponent implements OnInit {

  currentFilter: actions.validFilters = 'all';
  filters: actions.validFilters[] = ['all', 'completed', 'pending'];

  pending: number = 0;

  constructor( private store: Store<AppState> ) { }

  ngOnInit(): void {

    // this.store.select('filters')
    //   .subscribe( filter =>  this.currentFilter = filter );

    this.store.subscribe( state => {
      this.currentFilter = state.filters;
      this.pending = state.todos.filter( todo => !todo.done ).length;
    } )

  }

  changeFilter( filter: actions.validFilters ) {
    console.log(filter);
    this.store.dispatch( actions.setFilter( { filter }) )
  }

  clearCompleted() {  
    this.store.dispatch( clearCompleted( ) ) ;
  }

}
