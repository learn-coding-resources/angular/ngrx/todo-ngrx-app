import { state } from '@angular/animations';
import { createReducer, on } from '@ngrx/store';
import { Todo } from './models/todo.model';
import * as actions from './todo.actions';

export const initialState: Todo[] = [
    new Todo('Save the world!'),
    new Todo('Go to the target!'),
    new Todo('Learn NgRx!'),
    new Todo('Payment of domains')
];

const _todoReducer = createReducer(
  initialState,

  on(actions.create, ( state, { text } ) => [...state, new Todo( text )]),

  on(actions.remove, ( state, { id } )=> state.filter( todo => todo.id !== id  )),

  on(actions.clearCompleted, state => state.filter( todo => !todo.done ) ),

  on(actions.toggle, ( state, { id } ) => {
    return state.map( todo => {
      if (todo.id === id) {
        return {
          ...todo,
          done: !todo.done
        }        
      } else {
        return todo;
      }
    })
  }),

  on(actions.edit, ( state, { id, text } ) => {
    return state.map( todo => {
      if (todo.id === id) {
        return {
          ...todo,
          text: text
        }        
      } else {
        return todo;
      }
    })
  }),

  on(actions.toogleAll, ( state, {done} ) => {
    return state.map( todo => {
      return {
        ...todo,
        done: done
      }
    })
  })

);

export function todoReducer(state, action) {
  return _todoReducer(state, action);
}
