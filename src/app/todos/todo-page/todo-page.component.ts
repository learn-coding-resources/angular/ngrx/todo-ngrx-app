import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as actions from '../todo.actions';
import { AppState } from '../../app.reducer';
import { Todo } from '../models/todo.model';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.css']
})
export class TodoPageComponent implements OnInit {

  @Input() todo: Todo[];

  chkCompletedAllToggle: FormControl;
  completed: boolean = false;

  constructor( private store: Store<AppState> ) { }

  ngOnInit(): void {
    this.store.complete()
  }

  toogleAll() {
    this.completed = !this.completed;
    // console.log(this.completed);
    
    this.store.dispatch( 
      actions.toogleAll({ done: this.completed }) 
    );
    
  }

}
